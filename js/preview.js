(function (Drupal) {
  Drupal.behaviors.layoutBuilderAnimationsPreview = {
    attach: function (context) {
      once(
        "layoutBuilderAnimations",
        ".field--widget-layout-builder-animations-widget",
        context
      ).forEach((e) => {
        const previewContainer = e.querySelector(".preview");
        if (!previewContainer) {
          return;
        }

        const link = document.createElement("a");
        link.innerText = Drupal.t("Preview");
        link.classList.add("button");
        previewContainer.appendChild(link);
        previewContainer.style.aspectRatio = 1.4;

        const select = e.querySelector(
          "[name='settings[block_form][animation][0][animation]']"
        );

        const addPreview = function (parent) {
          const preview = document.createElement("div");
          preview.classList.add("inline-preview");
          preview.style.aspectRatio = 1.4;
          preview.style.width = "90%";
          preview.style.display = "inline-block";
          preview.style.boxSizing = "border-box";
          preview.style.backgroundColor = "white";
          preview.style.margin = "10px";
          preview.style.border = "1px solid black";

          parent.appendChild(preview);
          return preview;
        };

        const removePreview = function (parent) {
          const e = parent.querySelector(".inline-preview");
          if (e) {
            e.parentNode.removeChild(e);
          }
        };

        const previewAnimation = function (parent) {
          const duration = parent.querySelector(
            "[name='settings[block_form][animation][0][duration]']"
          );
          const name = parent.querySelector(
            "[name='settings[block_form][animation][0][animation]']"
          );
          removePreview(previewContainer);
          if (name.value === "") {
            return;
          }
          const preview = addPreview(previewContainer);
          preview.style.setProperty(
            "--animate-duration",
            duration.value + "ms"
          );
          preview.classList.add("animate__animated", `animate__${name.value}`);
        };

        link.onclick = () => {
          previewAnimation(e);
        };
        select.onchange = () => {
          previewAnimation(e);
        };
      });
    },
  };
})(Drupal, once);
