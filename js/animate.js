(function (Drupal) {
  let animations = [];
  const removeAnimation = function (e) {
    e.classList.remove(
      ...[`animate__animated`, `animate__${e.dataset.animationName}`]
    );
  };
  const addAnimation = function (e) {
    if (animations.indexOf(e) !== -1) {
      return;
    }
    animations.push(e);
    showEle(e);
    e.style.setProperty(
      "--animate-duration",
      e.dataset.animationDuration + "ms"
    );
    e.style.setProperty("animation-delay", e.dataset.animationDelay + "ms");
    e.classList.add(
      ...[`animate__animated`, `animate__${e.dataset.animationName}`]
    );
  };
  const hideEle = function (e) {
    if (animations.indexOf(e) !== -1) {
      return;
    }
    e.style.opacity = 0;
  };
  const showEle = function (e) {
    e.style.opacity = 1;
  };
  const isOutOfViewport = function (element) {
    if (!element) return false;
    if (1 !== element.nodeType) return false;

    var html = document.documentElement;
    var rect = element.getBoundingClientRect();

    return !(
      !!rect &&
      rect.bottom >= 0 &&
      rect.right >= 0 &&
      rect.left <= html.clientWidth &&
      rect.top <= html.clientHeight
    );
  };

  const hideAll = function (eles) {
    for (e in eles) {
      if (eles[e]) {
        hideEle(eles[e]);
      }
    }
  };
  Drupal.behaviors.layoutBuilderAnimations = {
    attach: function (context) {
      once("layoutBuilderAnimations", "html", context).forEach(function (
        element
      ) {
        const eles = Array.from(
          document.querySelectorAll("[data-animation-name]")
        );
        // Extra check for outside of viewport
        window.addEventListener(
          "scroll",
          () => {
            eles.forEach((e) => {
              if (isOutOfViewport(e)) {
                animations = animations.filter((ee) => ee != e);
                hideEle(e);
              }
            });
          },
          { passive: true }
        );
        const observer = new IntersectionObserver(
          function (entries, observer) {
            entries.forEach((e) => {
              if (e.isIntersecting) {
                addAnimation(e.target);
              }
            });
          },
          { threshold: 0.6 }
        );

        hideAll(eles);
        eles.forEach((e) => {
          e.addEventListener("animationend", function (ee) {
            removeAnimation(ee.target);
          });
          observer.observe(e);
        });
      });
    },
  };
})(Drupal, once);
