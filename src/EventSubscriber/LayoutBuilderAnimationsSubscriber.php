<?php

namespace Drupal\layout_builder_animations\EventSubscriber;

use Drupal\Core\Entity\FieldableEntityInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Drupal\layout_builder\Plugin\Block\InlineBlock;

/**
 * Layout_builder_animations event subscriber.
 */
class LayoutBuilderAnimationsSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      LayoutBuilderEvents::SECTION_COMPONENT_BUILD_RENDER_ARRAY => [
        'onBuildRender',
        -100,
      ],
    ];
  }

  /**
   * Event render function.
   */
  public function onBuildRender(SectionComponentBuildRenderArrayEvent $event) {
    $block = $block = $event->getPlugin();
    if ($event->inPreview()) {
      return;
    }
    if (!$block instanceof InlineBlock) {
      return;
    }
    $build = $event->getBuild();
    if (
      !isset($build['content'])
      || !isset($build['content']['#block_content'])
      || !($build['content']['#block_content'] instanceof FieldableEntityInterface)
      || !$build['content']['#block_content']->hasField('animation')
      || $build['content']['#block_content']->get('animation')->isEmpty()
    ) {
      return;
    }

    if (!isset($build['#attached']['library'])) {
      $build['#attached']['library'] = [];
    }

    $build['#attached']['library'][] = 'layout_builder_animations/animate.cdn';
    $build['#attributes']['data-animation-name'] = $build['content']['#block_content']->get('animation')->animation;
    $build['#attributes']['data-animation-duration'] = $build['content']['#block_content']->get('animation')->duration;
    $build['#attributes']['data-animation-delay'] = $build['content']['#block_content']->get('animation')->delay;
    $build['#attributes']['data-animation-easing'] = $build['content']['#block_content']->get('animation')->easing;

    $event->setBuild($build);
  }

}
