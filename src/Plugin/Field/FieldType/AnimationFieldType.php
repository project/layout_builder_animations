<?php

namespace Drupal\layout_builder_animations\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'baumeister_animation_field_type' field type.
 *
 * @FieldType(
 *   id = "layout_builder_animations_field_type",
 *   label = @Translation("Animation"),
 *   description = @Translation("Animation"),
 *   default_widget = "layout_builder_animations_widget",
 *   default_formatter = "layout_builder_animations_formatter"
 * )
 */
class AnimationFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [] + parent::defaultStorageSettings();
  }

  /**
   * Provide a list of animations.
   *
   * @return array
   *   Animation types.
   */
  public static function getAnimations() {
    return [
      '' => t('None'),
      (string) t('Attention Seekers') => [
        'bounce' => 'bounce',
        'flash' => 'flash',
        'pulse' => 'pulse',
        'rubberBand' => 'rubberBand',
        'shakeX' => 'shakeX',
        'shakeY' => 'shakeY',
        'headShake' => 'headShake',
        'swing' => 'swing',
        'tada' => 'tada',
        'wobble' => 'wobble',
        'jello' => 'jello',
        'heartBeat' => 'heartBeat',
      ],
      (string) t('Back entrances') => [
        'backInDown' => 'backInDown',
        'backInLeft' => 'backInLeft',
        'backInRight' => 'backInRight',
        'backInUp' => 'backInUp',
      ],
      (string) t('Bouncing entrances') => [
        'bounceIn' => 'bounceIn',
        'bounceInDown' => 'bounceInDown',
        'bounceInLeft' => 'bounceInLeft',
        'bounceInRight' => 'bounceInRight',
        'bounceInUp' => 'bounceInUp',
      ],
      (string) t('Fading entrances') => [
        'fadeIn' => 'fadeIn',
        'fadeInDown' => 'fadeInDown',
        'fadeInDownBig' => 'fadeInDownBig',
        'fadeInLeft' => 'fadeInLeft',
        'fadeInLeftBig' => 'fadeInLeftBig',
        'fadeInRight' => 'fadeInRight',
        'fadeInRightBig' => 'fadeInRightBig',
        'fadeInUp' => 'fadeInUp',
        'fadeInUpBig' => 'fadeInUpBig',
        'fadeInTopLeft' => 'fadeInTopLeft',
        'fadeInTopRight' => 'fadeInTopRight',
        'fadeInBottomLeft' => 'fadeInBottomLeft',
        'fadeInBottomRight' => 'fadeInBottomRight',
      ],
      (string) t('Rotating entrances') => [
        'rotateIn' => 'rotateIn',
        'rotateInDownLeft' => 'rotateInDownLeft',
        'rotateInDownRight' => 'rotateInDownRight',
        'rotateInUpLeft' => 'rotateInUpLeft',
        'rotateInUpRight' => 'rotateInUpRight',
      ],
      (string) t('Zooming entrances') => [
        'zoomIn' => 'zoomIn',
        'zoomInDown' => 'zoomInDown',
        'zoomInLeft' => 'zoomInLeft',
        'zoomInRight' => 'zoomInRight',
        'zoomInUp' => 'zoomInUp',
      ],
      (string) t('Sliding entrances') => [
        'slideInDown' => 'slideInDown',
        'slideInLeft' => 'slideInLeft',
        'slideInRight' => 'slideInRight',
        'slideInUp' => 'slideInUp',
      ],
      (string) t('Flippers') => [
        'flip' => 'flip',
        'flipInX' => 'flipInX',
        'flipInY' => 'flipInY',
      ],
      (string) t('Lightspeed') => [
        'lightSpeedInRight' => 'lightSpeedInRight',
        'lightSpeedInLeft' => 'lightSpeedInLeft',
      ],
      (string) t('Specials') => [
        'jackInTheBox' => 'jackInTheBox',
        'rollIn' => 'rollIn',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'animation';
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    // Treat the values as property value of the main property, if no array is
    // given.
    if (isset($values) && !is_array($values)) {
      $values = [static::mainPropertyName() => $values];
    }
    parent::setValue($values, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['animation'] = DataDefinition::create('string');
    $properties['duration'] = DataDefinition::create('integer');
    $properties['delay'] = DataDefinition::create('integer');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'animation' => [
          'type' => 'varchar',
          'length' => 64,
        ],
        'duration' => [
          'type' => 'int',
        ],
        'delay' => [
          'type' => 'int',
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('animation')->getValue();
    return $value === NULL || $value === '';
  }

}
