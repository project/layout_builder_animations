<?php

namespace Drupal\layout_builder_animations\Plugin\Field\FieldWidget;

use Drupal\layout_builder_animations\Plugin\Field\FieldType\AnimationFieldType;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'layout_builder_animations_widget' widget.
 *
 * @FieldWidget(
 *   id = "layout_builder_animations_widget",
 *   module = "layout_builder_animations",
 *   label = @Translation("Animations"),
 *   field_types = {
 *     "layout_builder_animations_field_type"
 *   }
 * )
 */
class AnimationFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['animation'] = [
      '#type' => 'select',
      '#title' => $this->t('Animation'),
      '#options' => AnimationFieldType::getAnimations(),
      '#default_value' => isset($items[$delta]->animation) ? $items[$delta]->animation : NULL,
    ];

    $element['delay'] = [
      '#type' => 'number',
      '#title' => $this->t('Delay'),
      '#default_value' => isset($items[$delta]->delay) ? $items[$delta]->delay : 0,
    ];

    $element['duration'] = [
      '#type' => 'number',
      '#title' => $this->t('Duration'),
      '#default_value' => isset($items[$delta]->duration) ? $items[$delta]->duration : 750,
    ];

    $element['preview'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['preview']],
      '#attached' => [
        'library' => [
          'layout_builder_animations/animate.cdn',
          'layout_builder_animations/preview',
        ],
      ],
    ];

    return $element += [
      '#type' => 'details',
    ];
  }

}
